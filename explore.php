<?php
/**
 *  All the jobs that should be done in cron-time
 * 
 * @author Aron Novak <aaron@szentimre.hu>
 * @version 0.1
 * @package sna
 */

/**
 * Get sql access and important functions
 */
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
require_once 'common.php';

$at_start = res_start();
if (!function_exists('dba_open') && SNA_CACHE_ENABLED) {
  die('Dba support is not available.');
}

clear_cache();
$edges = array();
$graph_source = variable_get('sna_data_source', GRAPH_SOURCE);
$build_edges = 'build_edges_from_' . $graph_source;
$num_interactions = $build_edges($edges);
$edges = transform_edges($edges);
if (!put_graph($edges)) {
  die('Cannot save the network into file');
}
/* Cache the most popular vertex's minimal tree */
$most_popular = sort_by_popularity($edges);
$most_popular_size = sizeof($most_popular);
$size_cache = variable_get('sna_size_cache', NUM_CACHE);
$limit = $most_popular_size < $size_cache ? $most_popular_size : $size_cache;
$at_start = res_start();
for ($i = 0; $i < $limit; $i++) {
  a_to_any($edges, $most_popular[$i][1], TRUE);
}
generate_graphviz_input($edges, $num_interactions);
generate_pajek_input($edges);

?>
