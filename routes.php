<?php
/**
 *  Finding minimal routes, BFS and DFS searches, collecting groups from the graph
 * 
 * @author Aron Novak <aaron@szentimre.hu>
 * @version 0.1
 * @package sna
 */

/**
 * Get sql access and important functions
 */
require_once 'common.php';

/**
 * Track back the route
 * 
 * @param array $result The minimal tree of routes
 * @param integer $to The vertex's id
 * @return array The route from $to to the root
 */
function rollback($result, $to) {
  
  $out = array();
  $prev = $to;
  $watch_infinite_loop = 0;
  while ($result['prev'][$prev] != '-') {
    $out[] = array('n' => $prev, 'd' => $result['dist'][$prev]);
    $prev = $result['prev'][$prev];
    if (++$watch_infinite_loop === 5000) {
      // It must be an infinite loop. This means that something terrible happens in the code elsewhere
      return array();
    }
  }
  return $out;
}

/**
 * Returns the shortest route from <var>$from</var> to <var>$to</var>
 * 
 * @param array $edges The adjacentcy list of the graph
 * @param integer $from The from vertex
 * @param integer $to The to vertex
 * @param boolean $step If False - Dijkstra, True - BFS
 */
function a_to_b($edges, $from, $to, $step = FALSE) {
  if ($from === $to) {
    return array();
  }
  if ($step === FALSE) {
    $min_tree = a_to_any($edges, $from);
  }
  else {
    $min_tree = breadth_first_walk($edges, $from, -1);
  }
  if (isset($min_tree['dist'][$to]) && $min_tree['dist'][$to] !== '-') {
    $route = rollback($min_tree, $to);
    $route[] = array('n' => $from, 'd' => 0);
    return $route;
  }
  else {
    return FALSE;
  }
}

/**
 * Measure the needed steps between two vertices
 *
 * @param array $edges The adjacentcy list of the graph
 * @param integer $from The from vertex
 * @param integer $to The to vertex
 * @return integer The count of needed steps
 */
function n_step_distance($edges, $from, $to) {
  static $cached, $cached_uid;
  if ($from === $to) {
    return 0;
  }
  if ($cached_uid === $from && is_array($cached)) {
    /* It's optimal caching because the average_step_separation function
       calls this function repeatedly */
    $min_tree = $cached;
  }
  else {
    $min_tree = breadth_first_walk($edges, $from, -1);
    $cached = $min_tree;
    $cached_uid = $from;
  }
  
  if (isset($min_tree['dist'][$to]) && $min_tree['dist'][$to] !== '-') {
    $route = rollback($min_tree, $to);
    return count($route);
  }
  else {
    return FALSE;
  }
}

/**
 * Compute the average step of separation in the network
 *
 * @param array $edges The adjacentcy list of the graph
 * @return integer Step of separation
 */
function average_step_separation($edges) {
  $users = get_all_vertices();
  $num = 0;
  foreach ($users as $uid1) {
    foreach ($users as $uid2) {
      if ($uid1 !== $uid2) {
        $dist = n_step_distance($edges, $uid1, $uid2);
        if ($dist !== FALSE) {
          $sum += $dist;
          ++$num;
        }
      }
    }
  }
  if ($num != 0) {  // Avoid division by zero!
    return $sum / $num;
  }
  else {
    return 0;
  }
}

/**
 * Read the graph from storage
 *
 * @param array_ref $edges This array will be filled with the graph's adjacentcy list
 */
function get_graph(&$edges) {
  if (variable_get('sna_realtime_network', FALSE) == TRUE) {
    clear_cache();
    $graph_source = variable_get('sna_data_source', GRAPH_SOURCE);
    $build_edges = 'build_edges_from_' . $graph_source;
    $build_edges($edges);
    $edges = transform_edges($edges);
  }
  else {
    include(DATA_PATH);
  }
  $limit = variable_get('sna_limit', 0);
  if ($limit > 0) {
    $edges = shrink($edges, $limit);
  }
  //print_r($edges);
}

/**
 * Breadt-first search algorithm
 *
 * @param graph $edges The adjacentcy list of the graph
 * @param vertex $start Starting vertex
 * @param integer $max Maximum step. -1 for no limit
 * @return array Result
 */
function breadth_first_walk($edges, $start, $max = -1) {
  // Set start values
  foreach ($edges as $key => $rel) {
    $c[$key] = 0;
    /* Color of point. 0 - white, 1 - grey, 2 - black
        white - not reached
        grey - reached
        black - reached and all neighbours is reached too
    */
    $d[$key] = '-';   // Distance from $start
    $p[$key] = '-';    // Previous vertex
    foreach (array_keys($rel) as $child) {
      $c[$child] = 0;
      $d[$child] = '-';
      $p[$child] = '-';
    }
  }
  $c[$start] = 1;
  $d[$start] = 0;
  $p[$start] = '-';
  $Q = array();
  $Q[] = $start;
  while (!empty($Q)) {
    foreach ($Q as $vk => $vertex) {
      if ($c[$vertex] === 1) {
        $u = $vertex;
        $u_k = $vk;
        break;
      }
    }
    if (($max !== -1) && $d[$u] + 1 > $max) {  // Reached the specified depth
      return array('dist' => $d, 'prev'  => $p);
    }
    $neighbours = is_array($edges[$u]) ? array_keys($edges[$u]) : array();
    foreach ($neighbours as $vert) {
      if ($c[$vert] === 0) {
        $c[$vert] = 1;
        $d[$vert] = $d[$u] + 1;
        $p[$vert] = $u;
        $Q[] = $vert;
      }
    }
    $c[$u] = 2;
    unset($Q[$u_k]);
  }
  return array('dist' => $d, 'prev'  => $p);
}

/**
 * Depth-first search algorithm
 *
 * @param array $edges The adjacentcy list of the graph
 */
function depth_first_walk($edges) {
  $all_vertices = get_all_vertices();
  foreach ($all_vertices as $u) {
    $c[$u] = 0;
    /* Color of point. 0 - white, 1 - grey, 2 - black
        white - not reached
        grey - reached
        black - reached and all neighbours is reached too
    */
    $p[$u] = '-';    // Previous vertex
  }
  $time = 0;
  foreach ($all_vertices as $u) { 
    if ($c[$u] === 0) {
      
      walk($u, $edges, $time, $c, $p, $f, $d);
      
    }
  }
  return array($f, $p);
}

/**
 * Depth-first search according to a previous BFS result (for interconnected parts)
 * 
 * @param array $edges The adjacentcy list of the graph
 * @param array $prev_res The previous BFS's results
 */
function mod_depth_first_walk($edges, $prev_res) {
  $prev_f = $prev_res[0];
  $p = $prev_res[1];
  $all_vertices = get_all_vertices();
  foreach ($all_vertices as $u) {
    $c[$u] = 0;
    /* Color of point. 0 - white, 1 - grey, 2 - black
        white - not reached
        grey - reached
        black - reached and all neighbours is reached too
    */
    $p[$u] = '-';    // Previous vertex
  }
  $time = 0;
  $prev_f_keys = empty($prev_f) ? array() : array_keys($prev_f);
  while ($u = array_pop($prev_f_keys)) {
    if ($c[$u] === 0) {
      walk($u, $edges, $time, $c, $p, $f, $d);
    }
  }
  return array($f, $p, $d);
}

/**
 * Helper function for BFS search algorithm
 *
 * @param integer $u The vertex's id
 * @param array $edges The adjacentcy list of the graph
 */
function walk($u, $edges, &$time, &$c, &$p, &$f, &$d) {
  
  $c[$u] = 1;
  $d[$u] = $time++;
  if (isset($edges[$u])) {
    foreach (array_keys($edges[$u]) as $v) {
      if ($c[$v] === 0) {
        $p[$v] = $u;
        walk($v, $edges, $time, $c, $p, $f, $d);
      }
    }
  }
  $c[$u] = 2;
  $f[$u] = $time++;
  
}

/**
 * Generate GML for outside tree visualization
 * Input for http://www.cs.ubc.ca/~sfingram/cs533C/small_world.html
 *
 * @param array $tree The BFS search result
 */
function show_tree($tree) {
  $num_of_edges = 0;
  foreach ($tree['prev'] as $vert => $prev) {
    if (is_numeric($prev)) {
      $map .= "<param name=\"edge". $num_of_edges++ ."\" value=\"". str_replace(' ', '_', get_real_name($prev)) . ' '  . str_replace(' ', '_', get_real_name($vert)) ."\">";
    }
  }
  $map .= "<param name=\"edgenum\" value=\"". $num_of_edges ."\">";
  return $map;
}

/**
 * Generate GML for outside network visualization
 * 
 *
 * @param array $edges The adjacentcy list of the graph
 * @return string The GML string
 */
function show_map($edges) {
  $num_of_edges = 0;
  foreach (array_keys($edges) as $A) {
    foreach (array_keys($edges[$A]) as $B) {
      $map .= "<param name=\"edge". $num_of_edges++ ."\" value=\"". str_replace(' ', '_', get_real_name($A)) . ' '  . str_replace(' ', '_', get_real_name($B)) ."\">";
      
    }
    
  }
  $map .= "<param name=\"edgenum\" value=\"". $num_of_edges ."\">";
  return $map;
}

/**
 * Transpose a graph.
 * $new grgraph is $edges with all its edges reversed.
 * 
 * @param array $edges The original graph
 * @return array $new_graph The transposed graph
 */
function transpose($edges) {
  $transposed_graph = array();
  foreach (array_keys($edges) as $A) {
    foreach (array_keys($edges[$A]) as $B) {
      $transposed_graph[$B][$A] = $edges[$A][$B];
    }
  }
  return $transposed_graph;
}

/**
 * Collect interconnected subgraphs according to two DFS search.
 *
 * @param array $prev The result of DFS
 * @return array The subgraphs (vertices list)
 */
function search_groups($edges) {
  
  $dfs_res = depth_first_walk($edges);
  
  $mod_dfs_res = mod_depth_first_walk(transpose($edges), $dfs_res);
  $f = $mod_dfs_res[0];
  $prev = $mod_dfs_res[1];
  if (empty($prev)) {
    return array();
  }
  $d = $mod_dfs_res[2];
  $d = empty($d) ? array() : $d;
  $f = empty($f) ? array() : $f;
  $groups = array();
  $index = 0;
  foreach (array_keys($prev) as $vert) {
    if ($prev[$vert] === '-') {
      $time = $d[$vert] + 1;
      $groups[$index][] = $vert;
      while ((($key = array_search($time, $d)) || array_search($time, $f)) && $prev[$key] !== '-') {
        if (!empty($key)) {
          $groups[$index][] = $key;
          unset($prev[$key]);
        }
        $time++;
      }
      $index++;
    
    }
  }
  return $groups;
 
}
/**
 * Throw away edges from an adjacentcy list according to edge weight
 *
 * @param array $edges The adjacentcy list of the graph
 * @return array The shrinked $edges graph
 */
function shrink($edges, $limit) {
  foreach (array_keys($edges) as $A) {
    foreach (array_keys($edges[$A]) as $B) {
      if ($edges[$A][$B] > $limit) {
        unset($edges[$A][$B]);
      }
    }
  }
  return $edges;
}

/**
 * Collect all the vertices degree
 *
 * @param array $edges The adjacentcy list of the graph
 */
function distribution_of_degree($edges) {
  $dist = array();
  foreach (array_keys($edges) as $vertex) {
    $dist[] = vertex_degree($edges, $vertex);
  }
  rsort($dist);
  return $dist;
}

/**
 * Draw a distribution of edges picture in SVG format.
 *
 * @param array $distribution
 * @return string The SVG picture file
 */
function visualize_distribution($distribution) {
  $max = $distribution[0];
  $height = count($distribution) == 0 ? 1 : count($distribution);
  $scale_height_factor = PIC_HEIGHT / $height;
  $scale_width_factor = PIC_WIDTH / $height;
  $svg_pic = '<svg width = "'. PIC_WIDTH .'px" height = "'. PIC_HEIGHT .'px" xmlns = "http://www.w3.org/2000/svg">';
  $svg_pic .= '<g transform="scale('. $scale_width_factor .' '. $scale_height_factor .')">';
  /* Draw the curve */
  $svg_pic .= '<polygon style="fill: #000000; stroke: #000000" points="';
  for ($i = 0; $i < $height; $i++) {
    if ($num !== 0) {
      $svg_pic .= ($i + 1) .",". ($height -($height * ($distribution[$i] / $max))) ." ";
    }
  }
  $svg_pic .= "0,". $height;
  $svg_pic .= ' " /> ';
  /* Create the captions */
  $svg_pic .= '<text x = "'.  $height * 0.01 .'" y = "'. $height * 0.09 .'"
               font-family = "Verdana" font-size = "'. $height / 30 .'" fill = "blue" >'. $max .'</text>';
  $svg_pic .= '</g></svg>';
  return $svg_pic;
}

/**
 * Watts-Strogatz: Clustering coefficient
 * The clustering coefficient for a vertex is the proportion of links between the vertices within
 * its neighbourhood divided by the number of links that could possibly exist between them. 
 *
 * @param integer $vertex
 * @param array $edges The adjacentcy list of the graph
 */
function clustering_coefficient($edges, $vertex) {
  if (!isset($edges[$vertex])) {
    if (function_exists('t')) { // Used in drupal, so we inform the user
      return t('Cannot compute clustering coefficient while user not have any connection.');
    }
    else {                      // Used in sna_test so possible to be rude :)
      return FALSE;
    }
  }
  /* Count the edges between the */
  $num_conn = 0;
  $neighbours = array_keys($edges[$vertex]);
  foreach ($neighbours as $vert) {
    if (is_array($edges[$vert])) {
      foreach (array_keys($edges[$vert]) as $vert_next) {
        if (array_search($vert_next, $neighbours) !== FALSE) {
          $num_conn++;
        }
      }
    }
  }
  $kn_vertices = num_vertices_of_complete_graph(count($neighbours));
  return $kn_vertices === 0 ? 0 : $num_conn / $kn_vertices;
}

/**
 * Compute the number of edges in Kn graph.
 *
 * @param integer $n Number of vertices
 * @return integer number of edges
 */
function num_vertices_of_complete_graph($n) {
  return $n * ($n - 1);
}

/**
 * Average shortest route
 *
 * @param unknown_type $edges
 * @return unknown
 */
function average_shortest_route($edges) {
  $all = get_all_vertices();
  $how_many_vertices = count($all);
  $how_many_probe = ceil(log($how_many_vertices) * 10);
  for ($i = 0; $i < $how_many_probe; $i++) {
    $rand1 = rand(0, $how_many_vertices - 1);
    $rand2 = rand(0, $how_many_vertices - 1);
    if ($rand1 !== $rand2) {
      print $rand1 ." ". $rand2 ."\n";
      $best_route = a_to_b($edges, $rand1, $rand2);
      $sum += count($best_route);
    }
  }
  return $sum / $how_many_probe;
}

?>
