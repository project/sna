<?php
/**
 * Common things in SNA related files
 * 
 * Store functions, constants that almost all file needs in SNA module
 * @author Aron Novak <aaron@szentimre.hu
 * @version 0.1
 * @package sna
 */

/**
 * The path of data files. This path can be modify trough drupal settings.
 */
define('FILES_PATH', '/path/to/a/secure/dir/');
/**
 * The path to the visualization applet
 */
define('APPLET_PATH', '/modules/sna/applet/');
/**
 * Set the dba_handler
 */
define('DBA_HANDLER', 'gdbm');

// Set some starting value
if (function_exists('variable_get')) {
  $files_path = variable_get('sna_data_path', FILES_PATH);
  $pic_size = variable_get('sna_pic_size', 300);
}
else {
  $files_path = FILES_PATH;
  $pic_size = 300;
}

/**
 * Here store the whole graph. Should be inaccessible for www user
 */
define('DATA_PATH', $files_path . 'ser.dat');
//define('DATA_PATH', './ser.dat');
/**
 * The Graphviz file's path
 */
define('DOT_PATH', $files_path . 'my.dot');
/**
 * The Pajek file's path
 */
define('NET_PATH', $files_path . 'my.net');
/**
 * The minimal tree of shortest route - cache
 */
define('CACHE_PATH', $files_path . 'min_trees');
/**
 * It's a very bad idea to rewrite this to FALSE.
 * If you do not have any dba_handler available in PHP
 * and you want to try out this module it's a possible
 * solution to turn this off.
 */
define('SNA_CACHE_ENABLED', TRUE);

/**
 * Possible options: nodes, buddy, stats
 */
define('GRAPH_SOURCE', 'nodes');
/**
 * At cron-time the script generate the top NUM_CACHE users minimal tree
 */
define('NUM_CACHE', '10');
/**
 * SVG picture width
 */
define('PIC_WIDTH', $pic_size);
/**
 * SVG picture height - same as width, we need 1:1 side proportion!
 */
define('PIC_HEIGHT', PIC_WIDTH);
define('ROUTE_SHORTEST', 0);
define('ROUTE_MIN_STEP', 1);
/**
 * Performace tester helper function. Start a timer and measure the current memory usage
 * 
 * @return array Startup measured time and memory usage
 */
function res_start() {
  static $timer_index;
  $mem_start = function_exists("memory_get_usage") ? memory_get_usage() : 0;
  timer_start($timer_index);
  return array(&$timer_index, $mem_start);
}

/**
 * Performace tester helper function. Compute the diff between res_start's figures.
 * 
 * @param array $at_start Startup measured time and memory usage
 */
function res_stop($at_start) {
  $res["time"] = timer_read($at_start[0]) / 1000 . " sec";
  $res["mem"] = function_exists("memory_get_usage") ? (memory_get_usage() - $at_start[1]) / 1024 . " kB" : "NaN";
  timer_stop($at_start[0]++);
  return $res;
}

/**
 * Lock the data file to avoid data corruption
 *
 * @param file_pointer $file_p
 * @return boolean success or not
 */
function lock($file_p) {
  while (!flock($file_p, LOCK_EX)) {
    $i++;
    usleep(10);
    if ($i > 10) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Get the nickname of a user
 *
 * @param user_id $uid Drupal uid of the user
 * @return nickname The uid's drupal nickname
 */
function get_real_name($uid) {
  $name_q = "SELECT name FROM {users} WHERE uid = %d";
  $name = db_query($name_q, $uid);
  $line = db_fetch_array($name);
  return $line["name"];
}

/**
 * Get from the database all the users' uid
 *
 * @return array All the users uid
 */
function get_all_vertices() {
  $vertices_q = "SELECT uid FROM {users} WHERE status = 1";
  $vertices = db_query($vertices_q);
  while ($line = db_fetch_array($vertices)) {
    $users[] = $line['uid'];
  }
  return $users;
}

/**
 * Get from the database all the users' uid and name to and assoc array
 *
 * @return array All the users uid and name
 */
function get_all_vertices_for_forms() {
  $vertices_q = "SELECT uid, name FROM {users} WHERE status = 1";
  $vertices = db_query($vertices_q);
  while ($line = db_fetch_array($vertices)) {
    $users[$line['uid']] = $line['name'];
  }
  return $users;
}

/**
 * Return the number of out-edges
 * 
 * @param array $edges The adjacentcy list of the graph
 * @param integer $vertex The vertex's id
 * @return integer The number of out-edges
 */
function vertex_degree($edges, $vertex) {
  return count($edges[$vertex]);
}

/**
 * Check if the minimal tree of <var>$vertex</var> is in cache or not
 * 
 * @param integer $vertex The vertex's id
 * @return boolean In cache or not
 */
function is_in_cache($vertex) {
  if (SNA_CACHE_ENABLED) {
    if (!$db = dba_open(CACHE_PATH, "c", DBA_HANDLER)) {
      die("Cannot open database\n");
    }
    $data = dba_fetch($vertex, $db);
    dba_close($db);
    return $data === FALSE ? FALSE : unserialize($data);
  } else {
    return FALSE;
  }
}

/**
 * Put the generated minimal tree to the cache
 * 
 * @param integer $vertex The vertex's id
 * @param array $data The minimal routes tree of $vertex
 * @return boolean Success or not
*/
function put_in_cache($vertex, $data) {
  if (SNA_CACHE_ENABLED) {
    if (!$db = dba_open(CACHE_PATH, "c", DBA_HANDLER)) {
      return FALSE;
    }
   
    if (!dba_insert($vertex, serialize($data), $db)) {
      dba_close($db);
      return FALSE;
    }
    dba_close($db);
    return TRUE;
  }
  else {  // If the caching is off, we imitate that everything works fine.
    return TRUE;
  }
}

/**
 * Find all the shortest routes from one vertex - Dijkstra algorithm
 * 
 * @param array $edges The adjacentcy list of the graph
 * @param integer $a  The vertex's id
 * @return array The minimal tree of routes
 */
function a_to_any($edges, $a, $in_explore = false) {
  if (!$in_explore) {
    if ($data = is_in_cache($a)) {
      return $data;
    }
  }
  
  /* Set start values and load all the vertices in Q row */
  $Q = array();
  foreach ($edges as $key => $rel) {
    $d[$key] = '-';   // Distance from $a
    $p[$key] = '-';
    $Q[] = $key;
    foreach (array_keys($rel) as $child) {
      $d[$child] = '-';   // Distance from $a
      $p[$child] = '-';
      $Q[] = $child;
    }
  }
  /* The distance of start point is 0 */
  $d[$a] = 0;
  $Q[] = $a;
  /* Delete duplicated elements */
  $Q = array_unique($Q);
  
  while (count($Q)) {
    /* Search the minimal d in Q */
    $u = reset($Q);
    $min = $d[$u];
    $min_key = key($Q);
    
    foreach ($Q as $vk => $vertex) {
      if (($min > $d[$vertex] || $min === '-') && is_numeric($d[$vertex])) {
        $min = $d[$vertex];
        $u = $vertex;
        $min_key = $vk;
      }
    }
    unset($Q[$min_key]);
    /* Test the edges of $u vertex */
    if (isset($edges[$u]) && $d[$u] !== '-') {
      foreach (array_keys($edges[$u]) as $next) {
        /* Test if get a shorter route - relaxation */
        $w = $edges[$u][$next];
        if ($d[$next] > $d[$u] + $w || $d[$next] === '-') {
          $d[$next] = $d[$u] + $w;
          $p[$next] = $u;
        }
      }
    }
  }
  $out = array('dist' => $d, 'prev' => $p);
  put_in_cache($a, $out);
  return $out;
}

/**
 * This is the graph's cost function
 * In a weighted graph or digraph,
 * each edge is associated with some value,
 * variously called its cost, weight, length
 * 
 * @param array $edges The adjacentcy list of the graph
 * @param integer $a From verticle
 * @param integer $b To verticle
 * @return integer The weight of the edge or FALSE if the edge is not set
 */
function get_edge_weight($edges, $a, $b, $min, $max) {
  if (isset($edges[$a][$b])) {
    /*
     * The function is a line. This line contains two points
     *     x1   y1                  x2    y2
     * P1 (min, max_length) and P2 (max, min_length)
     * min_length: 1 , max_length: 10
     * The line's equation : y - y1 = (y2 - y1) / (x2 - x1) (x - x1)
     * The result is a number between 1 and 10. 1 represents the
     * strongest connection.
     */
    if ($max != $min) {
      $y = (- 9 / ($max - $min) * ($edges[$a][$b] - $min)) + 10;
    }
    else {  // Avoid division by zero!
      $y = 1;
    }
    return $y;
  }
  else {  // No edge from a to b
    return FALSE;
  }
}

/**
 * Search the smallest and the biggest value in the <var>$edges</var> array that
 * represents connection strength
 *
 * @param array $edges The adjacentcy list of the graph
 * @return array The strength of the minimum and the maximum edge
 */
function get_min_and_max_strength($edges) {
  $degrees = array();
  foreach ($edges as $neighbours) {
    foreach ($neighbours as $degree) {
      $degrees[] = $degree;
    }
  }
  sort($degrees);
  return array(reset($degrees), end($degrees));
}

/**
 * Sort the edges by the number of out-edges
 *
 * @param array $edges The adjacentcy list of the graph
 * @param integer $limit Return the <var>$limit</var> - top of the users
 * @return array The sorted vertices list
 */
function sort_by_popularity($edges, $limit = -1) {
  $popularity = array();
  foreach (array_keys($edges) as $vertex) {
    $popularity[] = array(vertex_degree($edges, $vertex), $vertex);
  }
  usort($popularity, "compare");
  return $limit === -1 ? $popularity : array_slice($popularity, 0, $limit);
}

/**
 * Special compare function to sort multi-dimensional array easily
 *
 * @param array $a
 * @param array $b
 * @return boolean $b is greater than $a
 */
function compare($a, $b) {
  if ($a[0] < $b[0]) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Clear the dbm file that store the cached minimal routes
 *
 */
function clear_cache() {
  if (SNA_CACHE_ENABLED) {
    $db = dba_open(CACHE_PATH, "n", DBA_HANDLER);
    dba_close($db);
  }
}

/**
 * Create a graph from nodes-comments tables. 
 *
 * @param array $edges The adjacentcy list of the graph
 * @return integer The number of interactions
 */
function build_edges_from_nodes(&$edges) {
  $edges = array();
  $node_replies_q = "SELECT users.uid as u2, users_1.uid as u1
                     FROM {node} node, {users} users, {comments} comments, {users} users_1
                     WHERE comments.nid = node.nid AND node.uid = users.uid  AND users_1.uid = comments.uid
                     AND users.name <> '' AND users_1.name <> ''
                     AND comments.pid = 0";

  $comment_replies_q = "SELECT users_1.uid as 'u2', users.uid as 'u1'
                        FROM {users} users, {comments} comments, {comments} comments_1, {users} users_1
                        WHERE comments_1.cid = comments.pid
                        AND users.uid = comments.uid
                        AND users_1.uid = comments_1.uid
                        AND users_1.name <> ''
                        AND users.name <> ''";
  
  if ((!$node_replies = db_query($node_replies_q)) || (!$comment_replies = db_query($comment_replies_q))) {
    die("Database problem\n");
  }
  while ($line = db_fetch_array($comment_replies)) {
    if ($line["u1"] != $line["u2"]) { // Do not do hitches
      $edges[$line["u1"]][$line["u2"]]++;
    }
  }
  while ($line = db_fetch_array($node_replies)) {
    if ($line["u1"] != $line["u2"]) {
      $edges[$line["u1"]][$line["u2"]]++;
    }
  }
  return db_num_rows($node_replies) + db_num_rows($comment_replies);
}

/**
 * Create a graph from the buddylist module data
 *
 * @param array $edges The adjacentcy list of the graph
 * @return integer The numer of connections
 */
function build_edges_from_buddy(&$edges) {
  $edges = array();
  $buddy_q = "SELECT uid, buddy FROM {buddylist}";
  if ((!$buddies = db_query($buddy_q))) {
    die("Database problem\n");
  }
  while ($line = db_fetch_array($buddies)) {
    $edges[$line["uid"]][$line["buddy"]]++;
  }
  return db_num_rows($buddies);
}

/**
 * Create a graph from accesslog table. The connection is to view other's profile
 *
 * @param array $edges The adjacentcy list of the graph
 * @return integer The number of connections
 */
function build_edges_from_stats(&$edges) {
  $edges = array();
  $stats_q = "SELECT uid, path FROM {accesslog} WHERE path LIKE 'user/%'";
  if ((!$stats = db_query($stats_q))) {
    die("Database problem\n");
  }
  while ($line = db_fetch_array($stats)) {
    $dest = str_replace("user/", "", $line["path"]);
    if (is_numeric($dest) && $dest != $line["uid"]) {
      $edges[$line["uid"]][$dest]++;
    }
  }
  return db_num_rows($stats);
}

/**
 * Create a dot file from the graph to Graphviz
 * Graphviz is a graph visualization tool
 * 
 * @param array $edges The adjacentcy list of the graph
 * @param $num_interactions Number of edges in the graph
 * @return boolean The success of writing out the file
 */
function generate_graphviz_input($edges, $num_interactions) {
  $dot_graph = "digraph G {\n";
  foreach ($edges as $u1 => $sub_arr) {
    if ($u1 === 0) {    // Anonymous - don't count them!
      break;
    }
    foreach ($sub_arr as $u2 => $num) {
      if ($u2 === 0) {  // Anonymous - don't count them!
        break;
      }
      $dot_graph .= "\t\"". get_real_name($u1).
                    "\" -> \"". get_real_name($u2) .
                    "\" [label=". round($edges[$u1][$u2], 2) ."];\n";
    }
  }
  $dot_graph .= "}";
  if (!$fp = fopen(DOT_PATH, "w")) {
    return FALSE;
  }
  // Write out the DOT file
  fwrite($fp, $dot_graph);
  fclose($fp);
  return TRUE;
}

/**
 * Create a net file from the graph to Pajek
 * Pajek is a graph analizer and visualization tool
 * http://vlado.fmf.uni-lj.si/pub/networks/pajek/
 *
 * @param array $edges The adjacentcy list of the graph
 * @return boolean The success of writing out the file
 */
function generate_pajek_input($edges) {
  // Count unique vertex
  $edg = "*Edges\n";
  $vertex = get_all_vertices();
  $num_vertex = count($vertex);
  for ($i = 0; $i < $num_vertex; $i++) {
    $vert .= ($i + 1) ." \"". get_real_name($vertex[$i]) ."\"\n";
    // in NET files we have to index points in a strict order, uid is not suitable
    $real_id[$vertex[$i]] = $i + 1;
  }
  foreach ($edges as $vx_from => $next) {
    if ($vx_from === 0) {    // Anonymous - don't count them!
      break;
    }
    foreach (array_keys($next) as $vx_to) {
      if ($vx_to === 0) {    // Anonymous - don't count them!
        break;
      }
      $edg .= $real_id[$vx_from] ." ". $real_id[$vx_to] ." ". $edges[$vx_from][$vx_to] ."\n";
    }
  }
  $net .= "*Vertices ". $num_vertex ."\n". $vert . $edg;
  if (!$fp = fopen(NET_PATH, "w")) {
    return FALSE;
  }
  fwrite($fp, $net);
  fclose($fp);
  return TRUE;
}

/**
 * Write out the includable graph data
 * 
 * @param array $edges The adjacentcy list of the graph
 * @return boolean The success of writing out
 */
function put_graph($edges) {
  if (!$file_s = fopen(DATA_PATH, "w")) {
    return FALSE;
  }
  if (!lock($file_s)) {
    return FALSE;
  }
  fwrite($file_s, '<?php $edges = ' . var_export($edges, TRUE) . ';?>');
  fclose($file_s); 
  return TRUE;
}

/**
 * Convert all the edges cost<->length
 *
 * @param array $edges The adjacentcy list of the graph
 * @return array $edges The adjacentcy list of the graph
 */
function transform_edges($edges) {
  $min_max = get_min_and_max_strength($edges);
  $transformed_graph = array();
  foreach (array_keys($edges) as $A) {
    foreach (array_keys($edges[$A]) as $B) {
      $transformed_graph[$A][$B] = get_edge_weight($edges, $A, $B, $min_max[0], $min_max[1]);
    }
  }
  return $transformed_graph;
}

?>
